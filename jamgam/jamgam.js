// The MIT License (MIT)
//
// Copyright (c) 2017 Damian Balandowski
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE.

/**
 * Keyboard
 */
class Keyboard
{
    static released(key) {
        return released[key];
    }

    static pressed(key) {
        return pressed[key];
    }

    static key(_k) {
        return key[_k];
    }
}

/**
 * Mouse
 */
class Mouse
{
    static x() { return mouse_x; }
    static y() { return mouse_y; }

    static released()
    {
        return mouse_released;
    }

    static pressed()
    {
        return mouse_pressed;
    }

    static touchesNode(node, d = 64.0) {
        if(distance2(mouse_x, mouse_y, node.x, node.y) < d) {
            return true;
        }
        return false;
    }

    static clickedNode(node, d = 64.0) {
        if(Mouse.touchesNode(node,d) && Mouse.released())
        {
            return true;
        }

        return false;
    }
}

/**
 * Bitmap
 */
class Bitmap
{
    static load(path) {
        let bmp = load_bmp(path);
        return bmp;
    }

    static create(width, height) {
        return create_bitmap(width,height);
    }
}

/**
 * Sound
 */
class Sound
{
    constructor() {
        this.sample = null;
    }

    static create(filename) {
        let s = new Sound();
        s.sample = load_sample(filename);
        return s;
    }

    play() {
        play_sample(this.sample);
    }
}

/**
 * Node
 */
class Node
{
    constructor() {
        this.tag = 0;
        this.name = 'Node';
        this.x = 0.0;
        this.y = 0.0;
        this.rot = 0.0;
        this.vx = 0.0;
        this.vy = 0.0;
        this.size_x = 32.0;
        this.size_y = 32.0;
        this.layer = null;
        this.index = -1;
        this.camera = null;
    }

    removeFromLayer() {
        if(this.index > -1)
        {
            this.layer.removeNodeByIndex(this.index);
        }
    }

    distNode(node) {
        return distance2(this.x, this.y, node.x, node.y);
    }

    distPos(x,y) {
        return distance2(this.x, this.y, x, y);
    }

    move(x,y) {
        this.x += x;
        this.y += y;
    }

    moveX(x) {
        this.x += x;
    }

    moveY(y) {
        this.y += y;
    }

    rotate(deg) {
        this.rot += deg;
    }

    setRotation(rot) {
        this.rot = rot;
    }

    setVelocity(x,y) {
        this.vx = x;
        this.vy = y;
    }

    setVelocityX(x) {
        this.vx = x;
    }

    setVelocityY(y) {
        this.vy = y;
    }

    setPosition(x,y) {
        this.x = x;
        this.y = y;
    }

    setPositionX(x) { this.x = x; }
    setPositionY(y) { this.y = y; }

    getDrawPositionX() {
        if(this.camera != null) {
            return this.x - this.camera.x;
        }
        return this.x;
    }

    getDrawPositionY() {
        if(this.camera != null) {
            return this.y - this.camera.y;
        }
        return this.y;
    }

    setSize(x,y) {
        this.size_x = x;
        this.size_y = y;
    }

    setSizeX(x) {
        this.size_x = x;
    }

    setSizeY(y) {
        this.size_y = y;
    }

    update() {
        this.x += this.vx;
        this.y += this.vy;
    }
}

/**
 * Camera
 */
class Camera extends Node {}

/**
 * Pixel
 */
class Pixel extends Node
{
    constructor() {
        super();
        this.color = makecol(255,0,0);
    }

    static drawPixel(x,y,color) {
        putpixel ( canvas, x, y, color  );
    }

    static create(x, y, color) {
        let n = new Pixel();
        n.color = color;
        n.x = x;
        n.y = y;
        n.size_x = 2.0;
        n.size_y = 2.0;
        return n;
    }

    draw() {
        Pixel.drawPixel(this.getDrawPositionX(), this.getDrawPositionY(), this.color);
    }
}

/**
 * Sprite
 */
class Sprite extends Node
{
    constructor() {
        super();
        this.bitmap = null;
        this.sx = 1.0;
        this.sy = 1.0;
    }

    static create(bitmap) {
        let spr = new Sprite();
        spr.bitmap = bitmap;
        spr.setPosition(0,0);
        spr.size_x = bitmap.w;
        spr.size_y = bitmap.h;
        return spr;
    }

    setBitmap(newBitmap) {
        this.bitmap = newBitmap;
        this.size_x = this.bitmap.w;
        this.size_y = this.bitmap_h;
    }

    /**
     * [draw description]
     * @return {[type]} [description]
     */
    draw() {
        rotate_scaled_sprite(canvas,this.bitmap,this.getDrawPositionX(),this.getDrawPositionY(),this.rot,this.sx,this.sy);
    }
}

/**
 * Rect
 */
class Rect extends Node
{
    constructor() {
        super();
        this.color = makecol(255,0,0);
        this.w = 64;
        this.h = 64;
        this.fill = true;
        this.width = 1;
    }

    setBorderWidth(w)
    {
        this.width = w;
    }

    static create(width, height, color = makecol(255,0,0), fill = true, border_width = 1) {
        let n = new Rect();
        n.color = color;
        n.w = width;
        n.h = height;
        n.fill = fill;
        n.size_x = n.w;
        n.size_y = n.h;
        n.width = border_width;
        return n;
    }


    draw() {
        if(this.fill)
        {
            rectfill (canvas, this.getDrawPositionX(), this.getDrawPositionY(), this.w, this.h, this.color);
        }
        else {
            rect(canvas, this.getDrawPositionX(), this.getDrawPositionY(), this.w, this.h, this.color, this.width);
        }
    }
}

/**
 * Circle
 */
class Circle extends Node
{
    constructor() {
        super();
        this.radius = 1.0;
        this.width = 1.0;
        this.fill = true;
        this.color = makecol(255,0,0);
    }

    draw() {
        if(this.fill)
        {
            //circlefill (bitmap, x, y, radius, colour);
        }
        else
        {
            //circle (canvas, x, y, radius, colour, width);
        }
    }
}

/**
 * Text
 */
const textFont = create_font('arial');
class Text extends Node
{
    constructor() {
        super();
        this.text = "Text";
        this.font_size = 16;
        this.color = makecol(200,200,200);
        this.font = textFont;
    }

    setFont(fnt) {
        this.font = create_font(fnt);
    }

    setText(text) {
        this.text = text;
    }

    setFontSize(font_size) {
        this.font_size = font_size;
    }

    getFontSize(font_size) {
        return this.font_size;
    }

    draw() {
        textout_centre(canvas,this.font,this.text,this.getDrawPositionX(), this.getDrawPositionY(),this.font_size,this.color);
    }

    static create(text, x = 0, y = 0, color = makecol(150,150,150)) {
        let n = new Text();
        n.text = text;
        n.x = x;
        n.y = y;
        n.color = color;
        return n;
    }
}

/**
 * Line
 */
class Line extends Node
{
    constructor() {
        super();
        this.to_x = 0.0;
        this.to_y = 0.0;
        this.color = makecol(255,255,0);
    }

    static create(from_x, from_y, to_x, to_y, color) {
        let n = new Line();
        n.x = from_x;
        n.y = from_y;
        n.to_x = to_x;
        n_to_y = to_y;
        n.color = color;
        this.w = 1;
        return n;
    }

    draw() {
        line (canvas, this.x, this.y, this.to_x, this.to_y, this.color, this.w);
    }
}

/**
 * Layer
 */
class Layer
{
    constructor() {
        this.scene = null;
        this.nodes = [];
        this.enabled = true;
    }

    addNode(node) {
        this.nodes.push(node);
        node.layer = this;
        node.index = this.nodes.length;
        node.camera = this.scene.camera;
        return node;
    }

    setEnabled(enabled)
    {
        this.enabled = enabled;
    }

    removeNode(node) {
        if(node.layer == this)
        {
            for(let i = 0; i < this.nodes.length; i++)
            {
                if(this.nodes[i].index == node.index)
                {
                    this.removeNodeByIndex(this.nodes[i].index);
                }
            }
        }
    }

    removeNodeByIndex(index) {
        this.nodes.splice(index,1);
    }

    update() {
        if(this.enabled)
        {
            for(let i = 0; i < this.nodes.length; i++) {
                this.nodes[i].update();
            }
        }
    }

    draw() {
        if(this.enabled)
        {
            for(let i = 0; i < this.nodes.length; i++) {
                this.nodes[i].draw();
            }
        }
    }
}

/**
 * Scene
 */
class Scene
{
    constructor() {
        this.layers = [];
        this.camera = new Camera();

        let layer = this.newLayer();
    }

    initScene() {}
    updateScene() {}
    drawScene() {}

    /**
     * Init scene
     */
    init() {
        this.initScene();
    }

    deinit() {
        this.layers = [];
    }

    addLayer(layer) {
        this.layers.push(layer);
        layer.scene = this;
        return layer;
    }

    getLayer(index) {
        return this.layers[index];
    }

    newLayer() {
        let layer = new Layer();
        this.addLayer(layer);
        return layer;
    }

    addNode(node,layer_index=0) {
        this.layers[layer_index].addNode(node);
        return node;
    }

    update() {
        if(this.camera != null)
        {
            this.camera.update();
        }

        for(let i = 0; i < this.layers.length;i ++) {
            this.layers[i].update();
        }

        this.updateScene();
    }

    draw() {
        for(let i = 0; i < this.layers.length;i ++) {
            this.layers[i].draw();
        }

        this.drawScene();
    }
}

/**
 * Game
 */
class CGame
{
    constructor() {
        this.scene = new Scene();
        this.events = [];
        this.mouse_last_x = 0;
        this.mouse_last_y = 0;
        this.background = { r:30,g:30,b:30 };
        this.debug      = true;
    }

    getScene() {
        return this.scene;
    }

    draw() {
        this.scene.draw();
    }

    update() {
        if(mouse_x > this.mouse_last_x || mouse_x < this.mouse_last_x ||
            mouse_y > this.mouse_last_y || mouse_y < this.mouse_last_y)
        {
            this.call('mouse:moved', {mouse_x,mouse_y});
        }

        if(mouse_released)
        {
            this.call('mouse:released', {mouse_x, mouse_y});
        }

        if(mouse_pressed)
        {
            this.call('mouse:pressed', {mouse_x, mouse_y});
        }

        this.scene.update();

        this.mouse_last_x = mouse_x;
        this.mouse_last_y = mouse_y;
    }

    setScene(newScene) {
        this.scene.deinit();
        this.scene = newScene;
        this.scene.init();

        this.call('scene:changed', {scene:this.scene});
    }

    call(event_name, data) {
        for(let i = 0; i < this.events.length; i++)
        {
            if(event_name == this.events[i].name)
            {
                this.events[i].cb(data);
            }
        }
    }

    onEvent(name, cb) {
        this.events.push({name,cb});
    }
}

/**
 * Game instance
 */
const Game = new CGame();
