function main()
{
 if(Game.debug)
 {
     enable_debug('debug');
 }
 allegro_init_all("game_canvas", 640, 480);
 ready(function(){
     Game.call('load');
     Game.call('init');
     loop(function(){
         clear_to_color(canvas,makecol(Game.background.r,Game.background.g,Game.background.b));
         Game.update();
         Game.call('update');
         Game.call('draw');
         Game.draw();
     },BPS_TO_TIMER(60));
 });
 return 0;
}
END_OF_MAIN();
