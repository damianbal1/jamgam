
class GameScene extends Scene
{
    initScene() {
        this.text = Text.create('Hello world', 100, 100);
        this.addNode(this.text);

        this.r = Rect.create(64, 64, makecol(255,255,0), false, 3);
        this.r.setPosition(300,300);
        this.addNode(this.r);


    }

    updateScene() {

        //this.camera.x += 0.1;

        if(Mouse.touchesNode(this.r))
        {
            alert('klik!');
        }

        if(Keyboard.key(KEY_UP))
        {
            this.text.setFontSize(this.text.getFontSize() + 1);
            this.r.setBorderWidth(this.r.width + 1);
        }

        if(Keyboard.key(KEY_R))
        {
            //this.removeNode(this.text);
        }

        if(Keyboard.key(KEY_DOWN))
        {
            this.text.setFontSize(this.text.getFontSize() - 1);
        }
    }
}

class GameScene2 extends Scene
{
    initScene() {
        this.text = Text.create('<3', 100, 100);
        this.addNode(this.text);
    }
}


Game.onEvent('init', () =>
{
    let gs = new GameScene();
    Game.setScene(gs);
});
Game.onEvent('update', () => {

    if(Keyboard.key(KEY_SPACE))
    {
        let gs1 = new GameScene2();
        Game.setScene(gs1);
    }



});
Game.onEvent('draw', () => {});
